using UnityEngine;
using CloudOnce;

public class CloudOnceServices : MonoBehaviour
{

    #region singleton

    public static CloudOnceServices singleton { get; private set; }
    public void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }

    #endregion

    #region initialization

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    #region leaderboards

    public void SubmitScoreToLeaderboard(int score)
    {
        Leaderboards.HighScore.SubmitScore(score);
    }

    #endregion
}
