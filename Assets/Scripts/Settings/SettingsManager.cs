using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    public void SetPlayerSelection(int selection)
    {
        print($"settings manager selection change : {selection}");
        GameSettings.playerSelection = selection;
    }
}
