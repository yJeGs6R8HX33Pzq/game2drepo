using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour
{
    private string character3 = "com.davidfair.shapingup.character3";
    private string character4 = "com.davidfair.shapingup.character4";
    private string character5 = "com.davidfair.shapingup.character5";
    private string character6 = "com.davidfair.shapingup.character6";
    private string character7 = "com.davidfair.shapingup.character7";
    private string character8 = "com.davidfair.shapingup.character8";
    private string character9 = "com.davidfair.shapingup.character9";
    private string character10 = "com.davidfair.shapingup.character10";
    private string character11 = "com.davidfair.shapingup.character11";
    private string character12 = "com.davidfair.shapingup.character12";
    private string character13 = "com.davidfair.shapingup.character13";
    private string character14 = "com.davidfair.shapingup.character14";
    private string character15 = "com.davidfair.shapingup.character15";

    public void OnPurchaseComplete(Product product)
    {
        if (product.definition.id == character3)
        {
            StateManager.singleton.UnlockCharacter(2);
        }

        if (product.definition.id == character4)
        {
            StateManager.singleton.UnlockCharacter(3);
        }

        if (product.definition.id == character5)
        {
            StateManager.singleton.UnlockCharacter(4);
        }

        if (product.definition.id == character6)
        {
            StateManager.singleton.UnlockCharacter(5);
        }

        if (product.definition.id == character7)
        {
            StateManager.singleton.UnlockCharacter(6);
        }

        if (product.definition.id == character8)
        {
            StateManager.singleton.UnlockCharacter(7);
        }

        if (product.definition.id == character9)
        {
            StateManager.singleton.UnlockCharacter(8);
        }

        if (product.definition.id == character10)
        {
            StateManager.singleton.UnlockCharacter(9);
        }

        if (product.definition.id == character11)
        {
            StateManager.singleton.UnlockCharacter(10);
        }

        if (product.definition.id == character12)
        {
            StateManager.singleton.UnlockCharacter(11);
        }

        if (product.definition.id == character13)
        {
            StateManager.singleton.UnlockCharacter(12);
        }

        if (product.definition.id == character14)
        {
            StateManager.singleton.UnlockCharacter(13);
        }

        if (product.definition.id == character15)
        {
            StateManager.singleton.UnlockCharacter(14);
        }
    }




























    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(product.definition.id + " failed because " + failureReason);
    }

}
