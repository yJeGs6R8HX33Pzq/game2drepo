using UnityEngine;

/// <summary>
/// component that destroys an object at a specified interval after start
/// </summary>
public class TimedDestruction : MonoBehaviour
{
    [SerializeField] private float destructTime;
    private void Start()
    {
        Destroy(gameObject, destructTime);
    }
}
