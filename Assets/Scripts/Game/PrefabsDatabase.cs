using UnityEngine;

[CreateAssetMenu(menuName = "Database/PrefabsDatabase", fileName = "New Prefabs Database")]
public class PrefabsDatabase : ScriptableObject
{
    public GameObject[] resources;
}
