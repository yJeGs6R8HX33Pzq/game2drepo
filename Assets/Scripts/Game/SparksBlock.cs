using UnityEngine;

public class SparksBlock : Block
{
    [SerializeField] private GameObject destroyEffect;
    public override void CustomBlockBehaviour()
    {
        //instantiates a destroy effect
        GameObject sparks = Instantiate(destroyEffect, transform.position, Quaternion.identity);
    }
}
