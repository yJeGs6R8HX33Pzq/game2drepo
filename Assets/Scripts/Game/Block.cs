using UnityEngine;

public class Block : MonoBehaviour
{
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public virtual void CustomBlockBehaviour() { }

}

