using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    #region initialization
    public static GameManager singleton { get; private set; }
    [SerializeField] private Tracker cameraTracker;
    [SerializeField] private Tracker levelRigTracker;
    [SerializeField] private bool usesCheckpoints;

    //debug : check this bool to ignore game settings level id. used for manual testing of specific levels in the editor
    [SerializeField] private bool ignoreSettingsID;
    public bool isInfinity;

    private void Awake()
    {
        singleton = this;

        //activates the correct player based on the GameSettings selection index
        int selection = GameSettings.playerSelection;
        GameObject player = players[selection];
        players[selection].SetActive(true);

        //grabs a reference to the player object's player class
        Player plr = player.GetComponent<Player>();

        //sets the camera tracker target to this player
        cameraTracker.SetPlayer(plr);

        //sets the level rig tracker target to this player
        levelRigTracker.SetPlayer(plr);

        //sets position if using checkpoints
        if (usesCheckpoints)
        {
            if (!ignoreSettingsID)
            {
                levelID = GameSettings.levelIndex;
                currentLevel = levels[levelID];
                player.transform.position = LevelRig.singleton.transform.GetChild(levelID).position;
            }

            currentLevel = levels[levelID];

            //caches a reference to the start platform
            Transform startPlatform = LevelRig.singleton.transform.GetChild(levelID);

            //destroys the level up trigger on the first level so level up functionality is not called
            Destroy(startPlatform.GetComponent<LevelUp>());

            //sets the start platform collider to not is trigger
            startPlatform.GetComponent<Collider2D>().isTrigger = false;
        }
        else
        {
            if (isInfinity)
            {
            }
            else
            {
            //sets the current level
            currentLevel = levels[levelID];
            }
        }

        //subscribes the increment level method to the OnLevelUp delegate
        OnLevelUp += IncrementLevel;

        //updates the spawn probabilities
        UpdateSpawnProbabilities();

        prefabsDatabase = Resources.Load<PrefabsDatabase>("PrefabsDatabase");
        StartCoroutine(IntervalSpawn());

        //sets the min and max spawn time using the current level   
        minSpawnPause = currentLevel.spawnMin;
        maxSpawnPause = currentLevel.spawnMax;

        //sets the level height if not infinity mode
        if (!isInfinity)
        {
            SetLevelHeight();
        }
    }
    private void Start()
    {
        if (!isInfinity)
        {
            //initializes the goal score on the UIController
            UIController.singleton.UpdateGoalScore();
        }

        //initializes the level text display on the UIController
        UIController.singleton.UpdateLevelText();

        //subscribe a vibration to the level up
        OnLevelUp += Vibration.VibratePeek;

        //handles the secondary thread, if needed

        //handles the time scale
        Time.timeScale = currentLevel.timeConstant;

        OnStart();

        //if this is classic of hero mode, assign a stop all coroutines callback method to the on game complete delegate
        if (gameMode == GameMode.classic || gameMode == GameMode.hero)
        {
            //stops the spawning
            OnGameComplete += StopAllCoroutines;
            OnGameComplete += GameCompletionLogic;
        }

        if (gameMode == GameMode.hero)
        {
            //stops the spawning

            OnGameComplete += GameCompletionLogic;
            OnGameComplete += StopAllCoroutines;
            OnGameComplete += RewardPlayer;
        }
    }

    #endregion

    #region game mode

    public GameMode gameMode;

    #endregion

    #region levels

    //the index of the current level
    public int levelID = 0;

    public Level[] levels;

    //the current level
    public Level currentLevel;

    //a callback method for incrementing the level
    public delegate void LevelUpDelegate();
    public LevelUpDelegate OnLevelUp;

    private void IncrementLevel()
    {
        //sets the current level to the reference level, or increments the int and then sets the level.
        levelID += 1;
        currentLevel = levels[levelID];

        if (usesCheckpoints)
        {
            GameSettings.levelIndex = levelID;
        }

        //sets the new level height
        SetLevelHeight();

        //resets the current score
        score = 0;

        //resets the score ui display
        UIController.singleton.SetScore();

        //updates spawn times
        minSpawnPause = currentLevel.spawnMin;
        maxSpawnPause = currentLevel.spawnMax;

        //handles the time scale
        Time.timeScale = currentLevel.timeConstant;

        //updates spawn probabilities
        UpdateSpawnProbabilities();

        //call the virtual on start method in case this is an infinity game manager instance
        OnStart();
    }

    public virtual void OnStart() { }

    //getter for accessing the current level height

    public void SetLevelHeight()
    {
        currentLevelHeight = (int)LevelRig.singleton.transform.GetChild(levelID).position.y;
    }
    public int currentLevelHeight;

    #endregion

    #region procedural spawn

    [SerializeField] private float yOffset;
    [SerializeField] private float minSpawnPause;
    [SerializeField] private float maxSpawnPause;

    //the width of the screen
    [SerializeField] private float width;

    //the resources to spawn from (testing)
    public PrefabsDatabase prefabsDatabase;

    private Vector2 lastSpawnPosition;
    [SerializeField] private float minSpawnDistance;

    public float[] probabilities = new float[9];    

    //the main spawning thread
    private IEnumerator IntervalSpawn()
    {
        while (true)
        {
            //uses the random value to define a Vector2 spawnPosition
            Vector2 spawnPos = RandomSpawnPosition();

            //instantiates the prefab
            GameObject spawnObj = Instantiate(RandomSpawnPrefab(), spawnPos, Quaternion.identity);

            //selects a random time to pause until next instantiation using the min and max spawn pause
            float spawnPause = Random.Range(minSpawnPause, maxSpawnPause);

            //yeild return spawnPause seconds
            yield return new WaitForSeconds(spawnPause);
        }
    }

    private Vector2 RandomSpawnPosition()
    {
        //randomly selects and x value between -width /2 and width / 2
        float randX = Random.Range(-width / 2f, width / 2f) + Player.singleton.transform.position.x;

        //assigns the y value as the player height plus a constant y offset
        float yHeight = Player.singleton.transform.position.y + yOffset;

        Vector2 randPos = new Vector2(randX, yHeight);

        if (Vector2.Distance(randPos, lastSpawnPosition) < minSpawnDistance)
        {
            randPos = RandomSpawnPosition();
        }

        lastSpawnPosition = randPos;

        return randPos;
    }

    //uses the probabilities profile in the current level to select from the prefabs database.
    private GameObject RandomSpawnPrefab()
    {
        //declares a cumulative value tracker
        float cumulative = 0f;

        //selects a random value between 0 and 1
        float rand = Random.Range(0f, 1f);

        //adds the probabilities in the appopriate level blocks profile to the sum
        for (int i = 0; i < probabilities.Length; i++)
        {
            //increments the cumulative value, and then checks if the random value is less than that value - if so, returns the resources prefab at that index, else continues
            cumulative += probabilities[i];
            if (rand <= cumulative)
            {
                return prefabsDatabase.resources[i];
            }
        }


        //if no value was valid, debug and error msg and return
        Debug.Log("error! random prefab selection failed.");
        return null;
    }

    //updates the spawn probabilities using the level scriptable object - called from initialization and on level up
    public void UpdateSpawnProbabilities()
    {
        float[] currentProbabilities = currentLevel.GetProbabilities;

        for (int i = 0; i < probabilities.Length; i++)
        {
            probabilities[i] = currentProbabilities[i];
        }

        print("updates spawn probabilities!");
    }

    #endregion

    #region score

    public int score;
    public virtual void AdjustScore(float value)
    {
        score = (int)(value - currentLevelHeight);
        UIController.singleton.SetScore();
    }

    #endregion

    #region scene management

    public UIAnimHandler uiAnimHandler;
    public bool activeGame = true;
    public void ReturnToMain()
    {
        activeGame = false;
        uiAnimHandler.TransitionScene("Landing");

        //for now, reset the level id 
        GameSettings.levelIndex = 0;
    }

    public void LoadScene(string _target)
    {
        activeGame = false;
        uiAnimHandler.TransitionScene(_target);

        //for now, reset the level id 
        GameSettings.levelIndex = 0;
    }

    #endregion

    #region player selection

    [SerializeField] private GameObject[] players;

    #endregion

    #region game completion

    public delegate void GameCompletionCallback();
    public GameCompletionCallback OnGameComplete;

    public void RewardPlayer()
    {
        //gets the characters unlocked array
        bool [] charsUnlocked = StateManager.singleton.state.charactersUnlocked;

        //gets the heros unlocked array as a subset of the characters unlocked array
        bool[] herosUnlocked = new bool[5]
        {
            charsUnlocked[15],
            charsUnlocked[16],
            charsUnlocked[17],
            charsUnlocked[18],
            charsUnlocked[19],
        };

        for (int i = 0; i < herosUnlocked.Length; i++)
        {
            if (herosUnlocked[i] == false)
            {
                StateManager.singleton.state.charactersUnlocked[i + 15] = true;
                UIController.singleton.SetHeroReward(i);

                return;
            }
        }

        UIController.singleton.OnHeroCompletion();
        //if all heros are unlocked, display the legendary text in the UI controller
    }

    public void GameCompletionLogic()
    {
        //sets the active game to false
        activeGame = false;

        //sets the player object to inactive
        Player.singleton.gameObject.SetActive(false);

        //resets the Game Settings level index
        GameSettings.levelIndex = 0;
    }

    #endregion

}

public enum GameMode { classic, infinity, hero}
