using UnityEngine;

public class InfinityManager : GameManager
{
    new int levelID = 0;

    //when the score increases in the derived class, interpolate between your start level and end level probability to get your new probability profile
    public override void AdjustScore(float value)
    {
        //handles base score adjustment functionality
        base.AdjustScore(value);

        //interpolates between the start and end level scriptable objects for the new values
        float[] newProbabilities = new float[9];
        float progress = AdjustedScore / (float)levels[levelID].goal;

        for (int i = 0; i < newProbabilities.Length; i++)
        {
            float val = Mathf.Lerp(levels[levelID].GetProbabilities[i], levels[levelID + 1].GetProbabilities[i], progress);
            probabilities[i] = val;
        }

        //handles the time scale
        Time.timeScale = Mathf.Lerp(levels[levelID].timeConstant, levels[levelID + 1].timeConstant, progress);

        if (AdjustedScore >= currentLevel.goal)
        {
            //makes sure this is not the last level
            if (levelID < levels.Length - 1)
            {
                levelID += 1;
                currentLevel = levels[levelID];
            }
        }
    }

    //the score as a function of progress for this 'tier'.
    //score that gets 'reset to zero' so that progress for a tier can be tracked without affecting the actual score
    //used to determine progress for difficulty scaling, because values in infinity mode are interpolated, not changed in discrete 'OnLevelUp' events
    private float AdjustedScore
    {
        get
        {
            return ((float)score - levelID * 400);
        }
    }
}
