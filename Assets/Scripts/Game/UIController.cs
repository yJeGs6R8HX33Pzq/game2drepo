using UnityEngine;
using TMPro;
using System.Collections;

public class UIController : MonoBehaviour
{

    #region singleton

    public static UIController singleton { get; private set; }
    private void Awake()
    {
        singleton = this;
    }

    #endregion

    #region initialization

    private void Start()
    {
        bool isInfinity = GameManager.singleton.isInfinity;
        if (isInfinity == false)
        {
            GameMode gameMode = GameManager.singleton.gameMode;

            GameManager.singleton.OnLevelUp += DisplayLevelUpText;
            GameManager.singleton.OnLevelUp += UpdateGoalScore;
            GameManager.singleton.OnLevelUp += UpdateLevelText;

            if (gameMode == GameMode.classic)
            {
                GameManager.singleton.OnGameComplete += CompleteClassic;
            }
            else if (gameMode == GameMode.hero)
            {
                GameManager.singleton.OnGameComplete += CompleteHero;
            }
        }
    }

    #endregion

    #region graphics

    //retrieved by text objects for placement in world space on instantiation
    public RectTransform foreground;

    #endregion

    #region score

    [SerializeField] TMP_Text bestHeightDisplay;
    public void SetScore() 
    {
        int score = GameManager.singleton.score;
        bestHeightDisplay.text = score.ToString();
    }

    #endregion

    #region levels

    //the success text display for leveling up
    [SerializeField] private TMP_Text levelUpDisplayText;

    //the time parameters controlling the display animation for the level up display text
    [SerializeField] private float levelUpDisplayHoldTime;
    [SerializeField] private float levelUpDisplayFadeTime;

    //the target score text display for the level
    [SerializeField] private TMP_Text goalText;

    //the level display text
    [SerializeField] private TMP_Text levelDisplayText;

    //the amount of time to pause between writing level display characters
    [SerializeField] private float charDisplayPause;


    public void DisplayLevelUpText()
    {
        StartCoroutine(ExecuteDisplayLevelUpText());
    }
    private IEnumerator ExecuteDisplayLevelUpText()
    {
        //sets the level up display text color to white
        levelUpDisplayText.color = Color.white;

        //sets the level up text according to the new current level
        levelUpDisplayText.text = "Level " + (GameManager.singleton.levelID) + " complete!";

        //sets the levelUpDisplayText gameObject to active
        levelUpDisplayText.gameObject.SetActive(true);
        yield return new WaitForSeconds(levelUpDisplayHoldTime);

        //declares an elapsed time float
        float elapsedTime = 0f;

        //declares a progress float
        float progress = 0f;

        while (levelUpDisplayText.color != Color.clear)
        {
            //increments the elapsed time and progress
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / levelUpDisplayFadeTime;

            //lerps the color according to the progress
            levelUpDisplayText.color = Color.Lerp(Color.white, Color.clear, progress);

            //yields
            yield return new WaitForEndOfFrame();
        }

        //disables the display gameobject
        levelUpDisplayText.gameObject.SetActive(false);
    }


    //updates the goal text to display the goal score for the new level
    public void UpdateGoalScore()
    {
        goalText.text = "/" + GameManager.singleton.currentLevel.goal.ToString();
    }


    //updates the level display on level up
    public void UpdateLevelText()
    {
        if (GameManager.singleton.isInfinity) { return; }
        StartCoroutine(ExecuteUpdateLevelText());
    }

    private IEnumerator ExecuteUpdateLevelText()
    {
        string target = "LEVEL " + (GameManager.singleton.levelID + 1);
        levelDisplayText.text = "";

        foreach (char c in target)
        {
            levelDisplayText.text += c;
            yield return new WaitForSeconds(charDisplayPause);
        }
    }

    #endregion

    #region completion

    [Header("CLASSIC")]
    //maybe time this using a coroutine.
    [SerializeField] private GameObject completionPanel;

    [Header("HERO")]
    [SerializeField] private GameObject[] heros;
    [SerializeField] private GameObject legendaryText;
    [SerializeField] private GameObject rewardedText;

    public void CompleteClassic()
    {
        completionPanel.SetActive(true);
        goalText.gameObject.SetActive(false);
        bestHeightDisplay.gameObject.SetActive(false);
        levelDisplayText.gameObject.SetActive(false);
    }
    public void CompleteHero()
    {
        print("ui controller : complete hero!");
        completionPanel.SetActive(true);
    }

    public void SetHeroReward(int index)
    {
        rewardedText.SetActive(true);
        legendaryText.SetActive(false);
        heros[index].SetActive(true);
    }
    public void OnHeroCompletion()
    {
        rewardedText.SetActive(false);
        legendaryText.SetActive(true);
    }

    #endregion

    #region infinity

    [Header("INFINITY")]
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private TMP_Text finalScoreText;
    public void DisplayGameOverPanel()
    {
        //gets the score from the game manager singleton
        int score = GameManager.singleton.score;

        //sets the final score text on the game over panel
        finalScoreText.text = score.ToString() + "!";

        //sets the game over panel to inactive
        gameOverPanel.SetActive(true);

        //sets the camera tracker to inactive
        Camera.main.transform.parent.GetComponent<Tracker>().enabled = false;
    }

    #endregion

}
