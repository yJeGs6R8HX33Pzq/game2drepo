using UnityEngine;

using System.IO;

public class LinksManager : MonoBehaviour
{



	#region share with friends
	private string GetSubj()
	{
		return $"[Game Title] : score - 'current score' All time - {PlayerPrefs.GetFloat("HIGHSCORE", 0)} webpage or hashtag";
	}

	//the hyperlink to the app page
	private string url = "https://apps.apple.com/my/app/shaping-up/id1600288838";

	public void Share()
	{
		new NativeShare().SetText(GetSubj()).SetText(url).Share();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        //File.WriteAllBytes(filePath, iconImage.EncodeToPNG());
        new NativeShare().SetText(GetSubj()).AddFile(filePath).SetText(url).Share();
    }
	#endregion



	#region hyperlinks

	private string developerPage = "https://apps.apple.com/us/developer/david-fair/id1555551757?itsct=apps_box_link&itscg=30200";

	public void OpenPage()
	{
		Application.OpenURL(developerPage);
	}

	private string ccBYAttributionLink = "https://creativecommons.org/licenses/by/3.0/";

	public void OpenByLink(string _link)
	{
		Application.OpenURL(_link);
	}

	#endregion

}
