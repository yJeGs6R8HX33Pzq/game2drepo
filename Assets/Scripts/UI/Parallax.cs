using UnityEngine;

public class Parallax : MonoBehaviour
{
    //camera
    [SerializeField] private Transform cameraTransform;
    private Vector3 lastCameraPosition;

    //parallax
    [SerializeField] private float parallaxAmount;
    [SerializeField] private bool horizontalParallax;
    [SerializeField] private bool verticalParallax;

    //debug
    [SerializeField] private bool isTexture;

    //texture
    private float textureUnitSizeX;
    private float textureUnitSizeY;

    private void Start()
    {
        //handle camera params
        lastCameraPosition = cameraTransform.position;

        if (isTexture)
        {
        //handle texture params
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D tex = sprite.texture;
        textureUnitSizeX = tex.height / sprite.pixelsPerUnit;
        textureUnitSizeY = tex.width / sprite.pixelsPerUnit;
        }
        else
        {
            textureUnitSizeX = 0;
            textureUnitSizeY = 0;
        }

    }
    private void LateUpdate()
    {
        //handle reference fields
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        transform.position += deltaMovement * parallaxAmount;
        lastCameraPosition = cameraTransform.position;

        //handle horizontal parallax
        if (horizontalParallax)
        {
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX)
            {
                float offsetPostionX = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
                transform.position = new Vector3(cameraTransform.position.x, transform.position.y + offsetPostionX);
            }
        }
        
        //handle vertical parallax
        if (verticalParallax)
        {
            if (Mathf.Abs(cameraTransform.position.y - transform.position.y) >= textureUnitSizeY)
            {
                float offsetPostionY = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
                transform.position = new Vector3(transform.position.x, cameraTransform.position.y + offsetPostionY);
            }
        }
    }
}
