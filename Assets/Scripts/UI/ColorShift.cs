using UnityEngine;
using System.Collections;

public class ColorShift : MonoBehaviour
{
    //the material
    private Material material;

    //the current color, defaulting to white
    [SerializeField] private Color currentColor = Color.white;

    //the amount of time the background stays on the color
    [SerializeField] private float holdTime;

    //the amount of time that it takes to transition from one color to another
    [SerializeField] private float interpolationTime;

    //you may need to assign your shared material here
    private void Start()
    {
        material = GetComponent<SpriteRenderer>().material;

        //starts the coroutine to execute the color shift
        StartCoroutine(InterpolateColors());
    }
    private IEnumerator InterpolateColors()
    {
        //runs a loop to execute the color shift
        while (true)
        {
            yield return new WaitForSeconds(holdTime);

            //stores the current color value as the 'startColor' for use in the interpolation before starting the loop
            Color startColor = currentColor;

            //defines a new target color --- swap method here if needed
            Color targetColor = GetRandomColor();

            float elapsedTime = 0f;

            //while the current color is not equal to the target color, lerp from the starting current color to the target color of seconds 'interpolation time'
            while (currentColor != targetColor)
            {
                elapsedTime += Time.deltaTime;
                float percentInterpolated = elapsedTime / interpolationTime;

                currentColor = Color.Lerp(startColor, targetColor, percentInterpolated);
                material.SetColor("_BaseColor", currentColor);

                yield return new WaitForEndOfFrame();
            }
        }
    }

    [SerializeField] private Color[] colors;
    private Color GetRandomColor()
    {
        Color newColor = colors[Random.Range(0, colors.Length)];
        if (newColor == currentColor)
        {
            newColor = GetRandomColor();
        }
        return newColor;
    }
}
