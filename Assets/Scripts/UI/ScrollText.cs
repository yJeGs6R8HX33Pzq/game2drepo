using UnityEngine;
using TMPro;

public class ScrollText : MonoBehaviour
{
    //for text display
    public TMP_Text text;

    //for determining positioning
    private RectTransform rect;
    private RectTransform canvasRect;
    private Vector2 uiOffset;

    public void Initialize(Vector3 worldPos)
    {
        //sets the rect and canvas rect values
        rect = GetComponent<RectTransform>();
        canvasRect = UIController.singleton.foreground;

        transform.parent = canvasRect;

        //determines the offset
        uiOffset = new Vector2(canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);

        //determines viewport positions
        Vector2 viewportPosition = Camera.main.WorldToViewportPoint(worldPos);
        Vector2 proportionalPosition = new Vector2(viewportPosition.x * canvasRect.sizeDelta.x, viewportPosition.y * canvasRect.sizeDelta.y);

        //sets the position
        rect.localPosition = proportionalPosition - uiOffset;

        //unparent the object
        transform.parent = null;

        //destroy the object after one second
        Destroy(gameObject, 1);
    }    
    
    public void SetValue(string textToDisplay, bool isStreaking)
    {
        text.text = textToDisplay;

        if (isStreaking)
        {
            transform.localScale *= 1.5f;
        }
    }
}
