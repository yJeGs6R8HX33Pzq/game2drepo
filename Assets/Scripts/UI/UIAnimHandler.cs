using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIAnimHandler : MonoBehaviour
{

    [SerializeField] private Image img;
    [SerializeField] private float fadeTime;

    //colors
    private Color blackColor = new Color(0, 0, 0, 1);
    private Color clearColor = new Color(0, 0, 0, 0);


    #region initialization

    private void Start()
    {
        StartCoroutine(BlackToClear());
    }

    #endregion

    #region fading

    //this is called from a button that triggers a fade in order to swap the active state of two ui objects
    public void TransitionPanel(SwapItems swapItems)
    {
        StopAllCoroutines();
        StartCoroutine(ExecutePanelTransition(swapItems.object1, swapItems.object2));
    }

    //executes the panel transition
    private IEnumerator ExecutePanelTransition(GameObject currentPanel, GameObject targetPanel)
    {
        print("calling clear to black");
        StartCoroutine(ClearToBlack());

        yield return new WaitForSeconds(fadeTime);
        currentPanel.SetActive(false);
        targetPanel.SetActive(true);

        StartCoroutine(BlackToClear());
    }

    //this is called from a button in order to transition scenes after a fade
    public void TransitionScene(string targetScene)
    {
        StartCoroutine(ExecuteSceneTransition(targetScene));
    }

    //executes the scene fade transition
    private IEnumerator ExecuteSceneTransition(string targetScene)
    {
        StartCoroutine(ClearToBlack());
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(targetScene);

    }

    private IEnumerator BlackToClear()
    {
        img.enabled = true;
        float elapsedTime = 0;
        while (img.color != clearColor)
        {
            elapsedTime += Time.deltaTime;
            float t = elapsedTime / fadeTime;
            img.color = Color.Lerp(blackColor, clearColor, t);


            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator ClearToBlack()
    {

        float elapsedTime = 0;
        while (img.color != blackColor)
        {
            elapsedTime += Time.deltaTime;
            float t = elapsedTime / fadeTime;
            img.color = Color.Lerp(clearColor, blackColor, t);

            yield return new WaitForEndOfFrame();
        }
    }

    #endregion

    #region popup

    private Vector3 overshootSize = new Vector3(1.1f, 1.1f, 0);
    [SerializeField] private float mainTime;

    private Vector3 openSize = new Vector3(1, 1, 0);
    [SerializeField] private float tailTime;

    public void PopupOpen(RectTransform target)
    {
        //start the execute popup open coroutine
        StartCoroutine(ExecutePopupOpen(target));
    }
    private IEnumerator ExecutePopupOpen(RectTransform target)
    {
        //set the target gameobject to active
        target.gameObject.SetActive(true);

        //set the target scale to zero
        target.localScale = Vector2.zero;

        //declares an elapsed time float
        float elapsedTime = 0f;

        //declares a progress float
        float progress;

        //scale the target up to Vector2.One
        while (target.localScale != overshootSize)
        {
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / mainTime;
            target.localScale = Vector3.Lerp(Vector3.zero, overshootSize, progress);

            yield return new WaitForEndOfFrame();
        }

        //resets the progress tracker float values
        elapsedTime = 0f;
        progress = 0f;

        while (target.localScale != openSize)
        {
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / tailTime;
            target.localScale = Vector3.Lerp(overshootSize, openSize, progress);

            yield return new WaitForEndOfFrame();
        }
    }

    public void PopupClose(RectTransform target)
    {
        //start the execute popup close coroutine
        StartCoroutine(ExecutePopupClose(target));
    }
    private IEnumerator ExecutePopupClose(RectTransform target)
    {
        //set the target gameobject to active
        target.gameObject.SetActive(true);

        //set the target scale to zero
        target.localScale = Vector2.zero;

        //declares an elapsed time float
        float elapsedTime = 0f;

        //declares a progress float
        float progress;

        //scale the target up to Vector2.One
        while (target.localScale != overshootSize)
        {
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / tailTime;
            target.localScale = Vector3.Lerp(openSize, overshootSize, progress);

            yield return new WaitForEndOfFrame();
        }

        //resets the progress tracker float values
        elapsedTime = 0f;
        progress = 0f;

        while (target.localScale != Vector3.zero)
        {
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / mainTime;
            target.localScale = Vector3.Lerp(overshootSize, Vector3.zero, progress);

            yield return new WaitForEndOfFrame();
        }

        target.gameObject.SetActive(false);
    }

    #endregion
}
