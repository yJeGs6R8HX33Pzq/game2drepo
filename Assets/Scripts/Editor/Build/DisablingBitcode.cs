using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditor.Callbacks;
using System.IO;

public class DisablingBitcodeiOS
{
    [PostProcessBuild(1000)]    //evaluate whether the order value is significant after the build 
    public static void PostProcessBuildAttribute(BuildTarget target, string pathToBuildProject)
    {
        if (target == BuildTarget.iOS)
        {
            string projectPath = PBXProject.GetPBXProjectPath(pathToBuildProject);
            PBXProject proj = new PBXProject();
            proj.ReadFromFile(projectPath);

            string targetGuid = proj.GetUnityMainTargetGuid();

            proj.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");
            proj.WriteToFile(projectPath);

            var projectInString = File.ReadAllText(projectPath);
            projectInString = projectInString.Replace("ENABLE_BITCODE = YES;", $"ENABLE_BITCODE = NO;");
            File.WriteAllText(projectPath, projectInString);

            /*
             * alternatively here, proj.WriteToFile(projPath);
             */
        }
    }
}


