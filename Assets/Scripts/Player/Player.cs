using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Player : MonoBehaviour
{

    #region singleton

    public static Player singleton { get; private set; }
    private void Awake()
    {
        singleton = this;
    }

    #endregion

    #region initialization

    [SerializeField] private SpriteRenderer sr;
    Color baseColor;


    private void Start()
    {
        //initialize the best height
        bestHeight = transform.position.y;

        //assign handling callbacks to the game manager (game completeion)
        GameMode gameMode = GameManager.singleton.gameMode;

        material = sr.material;

        //caches a reference to the base color
        baseColor = sr.material.color;
    }

    #endregion

    #region input

    //the amount of force applied to the player on flick
    [SerializeField] private float flickForce;

    //the starting position of a touch sequence
    private Vector2 startTouch;

    #endregion

    #region control

    private bool canFlick = true;
    private void Update()
    {
        #region touches

        if (Input.touchCount == 1)
        {
            Touch t = Input.touches[0];
            if (t.phase == TouchPhase.Began)
            {
                startTouch = t.position;
            }
            else if (t.phase == TouchPhase.Ended && canFlick == true)
            {
                //gets the direction of the swipe (normalized for now)
                Vector2 dir = (t.position - startTouch);

                //applies force to the rigidbody
                rb2D.AddForce(dir * flickForce);

                //mess around with the torque a bit
                var impulse = (Vector2.SignedAngle(transform.up, dir) * Mathf.Deg2Rad) * rb2D.inertia;
                rb2D.AddTorque(impulse, ForceMode2D.Impulse);

                //sets can flick to false
                canFlick = false;

                //play the flick audio clip
                GameAudioManager.singleton.PlayFlick();

                //sets the emission to regular
                SetMaterialActivation(false);
            }
        }

        //get the rb2D speed
        float speed = rb2D.velocity.magnitude;
        //set the trail particles to emitting or not emitting based on the speed threshold check
        var emission = trailParticles.emission;
        emission.enabled = speed > trailParticlesThreshold;

        #endregion

        #region score handling

        //updates the best height if needed
        if (transform.position.y > bestHeight)
        {
            bestHeight = transform.position.y;
            GameManager.singleton.AdjustScore(bestHeight);
        }

        #endregion
    }
    #endregion

    #region physics

    [SerializeField] private Rigidbody2D rb2D;
    [SerializeField] private GameObject scrollDisplay;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.gameObject.tag == "Danger")
        //{
        //    OnFail();
        //}

        //sets can flick to true
        canFlick = true;

        //handles the emission graphic
        SetMaterialActivation(true);

        //if the velocity magnitude is great enough and enough time has elapsed since the last spark instantiation, instantiation sparks at the contact point
        if (rb2D.velocity.magnitude > 3 && canMakeSparks)
        {
            CreateSparks(collision.contacts[0].point);

            Block block = collision.collider.GetComponent<Block>();
            if (block != null) 
            {
                OnBlockCollision(block);
            }       
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IInteractable[] iInteractables = collision.GetComponentsInChildren<IInteractable>();
        if (iInteractables.Length > 0)
        {
            foreach (IInteractable iInteractable in iInteractables)
            {
                iInteractable.Interact();
            }
        }
    }

    #endregion

    #region scoring

    public float bestHeight;
    private string[] successDisplays = new string[]
    {
        "Nice!",
        "Sweet",
        "Solid!",
        "Good",
        "Great!",
        "Good One",
        "Awesome!",
        "Phenomenal!",
        "Solid",
        "Nice",
        "Excellent",
        "Amazing!"
    };

    //called when a block is collided with
    private void OnBlockCollision(Block block)
    {
        //handles any custom behaviour on the block
        block.CustomBlockBehaviour();

        //handle the scroll text
        GameObject display = Instantiate(scrollDisplay);
        ScrollText scrollText = display.GetComponent<ScrollText>();
        scrollText.Initialize(transform.position);
        scrollText.SetValue(successDisplays[Random.Range(0, successDisplays.Length)], false);

        //create a vibration
        Vibration.VibratePop();

        //play the hit sound clip
        GameAudioManager.singleton.PlayHit(rb2D.velocity.magnitude);

    }
 
    #endregion

    #region effects

    [SerializeField] private GameObject sparks;
    [SerializeField] private ParticleSystem trailParticles;
    [SerializeField] private float trailParticlesThreshold;

    private Material material;
    private const float INACTIVE = .7f;
    private const float ACTIVE = 3.8f;

    private void CreateSparks(Vector2 pos)
    {
        StartCoroutine(SparksTimer());

        //you may want to pool these so that you're not constantly instantiating, but this should work for prototyping.
        GameObject sp = Instantiate(sparks, pos, Quaternion.identity);
        Destroy(sp, 3);
    }

    private float sparksPause = .15f;
    private bool canMakeSparks = true;
    private IEnumerator SparksTimer()
    {
        canMakeSparks = false;
        yield return new WaitForSeconds(sparksPause);
        canMakeSparks = true;
    }

    private void SetMaterialActivation(bool state)
    {
        material.SetVector("_Color", state == true ? baseColor * ACTIVE : baseColor * INACTIVE);
    }

    #endregion

    #region completion

    public void OnClassicComplete()
    {
        enabled = false;
        //disables the controller

        //fades to white?
        //animation upward?
        //fade to thanks panel and prompt?
    }

    public void OnHeroComplete()
    {
        //disables the controller
        enabled = false;


        //fades to white?
        //animation upward?
        //rolls for character using state or settings data
    }

    #endregion 

    #region fail condition

    private void OnBecameInvisible()
    {
        //checks the active state of the game and fails if is an active game - this allows for scene transitions if the game is not active, which is the case when the home button is called
        if (GameManager.singleton.activeGame == true)
        {
            OnFail();
        }
    }

    private void OnFail()
    {
        bool isInfinity = GameManager.singleton.isInfinity;
        if (isInfinity == false)
        {
            string loadKey = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(loadKey);
        }
        else
        {
            //handles game center logic
            int score = GameManager.singleton.score;
            CloudOnceServices.singleton.SubmitScoreToLeaderboard(score);

            //handles UI
            UIController.singleton.DisplayGameOverPanel();
        }
    }

    #endregion
}
