using UnityEngine;

public class Tracker : MonoBehaviour
{
    public Player player;
    [SerializeField] private float yOffest;
    [SerializeField] private bool horizontal;
    [SerializeField] private bool vertical;

    float x;
    float y;
    float zConst;

    private void Start()
    {
        zConst = transform.position.z;
    }
    private void Update()
    {
        //assigns value if horizontal
        if (horizontal)
        {
            x = player.transform.position.x;
        }

        //assigns value if vertical
        if (vertical)
        {
            y = player.bestHeight + yOffest;
        }

        //determines the target
        Vector3 target = new Vector3(horizontal ? x : transform.position.x, vertical ? y : transform.position.y, zConst);

        //sets the position to the target
        transform.position = target;
    }

    #region player

    public void SetPlayer(Player plr)
    {
        player = plr;
    }

    #endregion
}
