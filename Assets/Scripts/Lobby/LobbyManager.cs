using UnityEngine;
using TMPro;

public class LobbyManager : MonoBehaviour
{

    #region singleton

    public static LobbyManager singleton { get; private set; }
    private void Awake()
    {
        singleton = this;
    }

    #endregion

    #region initialization

    private void Start()
    {
        //updates the IAP buttons
        UpdateIAPButtons();

        //handles premium (hero mode)
        HandlePremium();
    }

    #endregion

    #region scene

    //a reference to the lobby uiAnimHandler
    [SerializeField] private UIAnimHandler uIAnimHandler;

    //the main scene game mode button display text
    [SerializeField] private TMP_Text gameModeDisplay;

    //the target scene to load, set by a game mode button
    private string targetScene = "Infinity";
     
    //a method for setting the target scene
    public void SetTargetScene(string _targetScene)
    {
        //sets the local target scene reference
        targetScene = _targetScene;

        //updates the game mode display text using the new target scene string
        gameModeDisplay.text = _targetScene;
    }

    //a method for transitioning scenes, using the local target reference
    public void TransitionScene()
    {
        uIAnimHandler.TransitionScene(targetScene);
    }
    #endregion

    #region iap

    [SerializeField] public GameObject[] iapButtons;
    [SerializeField] public TMP_Text characterCountDisplay;

    public void UpdateIAPButtons()
    {
        if (StateManager.singleton == null)
        {
            print("null state");
        }
        else
        {
            print("success state");
        }



        bool[] unlockStates = StateManager.singleton.state.charactersUnlocked;
        int unlocked = 0;

        for (int i = 0; i < 20; i++)
        {
            iapButtons[i].SetActive(!unlockStates[i]);
            if (unlockStates[i] == true)
            {
                unlocked += 1;
            }
        }

        characterCountDisplay.text = unlocked + "/20";
    }

    #endregion

    #region premium

    [SerializeField] private GameObject premiumPrompt;
    [SerializeField] private GameObject heroButtonActive;


    public void HandlePremium()
    {
        bool[] charsUnlocked = StateManager.singleton.state.charactersUnlocked;
        for (int i = 2; i < charsUnlocked.Length; i++)
        {
            if (charsUnlocked[i] == true)
            {
                DisablePremiumPrompt();
                UnlockHeroMode();
            }
        }
    }

    private void DisablePremiumPrompt()
    {
        premiumPrompt.SetActive(false);
    }
    private void UnlockHeroMode()
    {
        heroButtonActive.SetActive(true);
    }

    #endregion

}
