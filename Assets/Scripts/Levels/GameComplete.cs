using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameComplete : MonoBehaviour, IInteractable
{
    public void Interact()
    {
        GameManager.singleton.OnGameComplete?.Invoke();
        Destroy(this);
    }
}
