using UnityEngine;

[CreateAssetMenu(menuName = "Level/Level", fileName = "New Level")]
public class Level : ScriptableObject
{
    //the target score & height to attain to reach the next level
    [Header("OBJECTIVE")]
    public int goal;

    //the gravity value applied to blocks on instantiation
    [Header("Time")]
    public float timeConstant;

    [Header("GRAPHICS")]
    public Material backdropMaterial;

    [Header("SPAWN PARAMETERS")]
    public float spawnMin;
    public float spawnMax;

    [Header("BLOCKS")]
    //standard blocks, run on the main spawning thread in game manager
    public float probabilityGreen;  //big, green blocks
    public float probabilityPink;   //medium, pink blocks
    public float probabilityOrange; //small, orange blocks
    public float probabilityRed;    //rectangular, red blocks that pop
    public float probabilityPurple; //small, but extra bouncy blocks
    public float probabilityGrey;   //hard, immovable grey blocks
    public float probabilityTeal;   //small, freezy blocks
    public float probabilityBlack;  //gravity inward circle
    public float probabilityWhite;  //gravity outward circle

    //secondary blocks, run on the secondary spawning thread in the game manager

    [HideInInspector]
    public float[] GetProbabilities
    {
        get
        {
            return new float[9] 
            {
                probabilityGreen,
                probabilityPink,
                probabilityOrange,
                probabilityRed,
                probabilityGrey,
                probabilityPurple,
                probabilityTeal,
                probabilityBlack,
                probabilityWhite

            };
        }
    }
}
