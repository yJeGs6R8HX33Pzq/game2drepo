using UnityEngine;

public class LevelUp : MonoBehaviour, IInteractable
{
    public void Interact()
    {
        GameManager.singleton.OnLevelUp?.Invoke();
        Destroy(this);
    }
}
