using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
    Level level;
    private void OnEnable()
    {
        level = (Level)target;
    }

    //to do :
    //design the editor such that a range slider change on any of the parameters would affect all other non-locked fields, so long as at least one other field were locked.
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}

