using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelRig))]
public class LevelsRigEditor : Editor
{
    LevelRig levelRig;
    const float playerWidth = 1f;

    private void OnEnable()
    {
        //caches the LevelRig reference using target
        levelRig = (LevelRig)target;

        //organizes the level positions
        PositionLevels();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.color = Color.green;
        if (GUILayout.Button("NEW LEVEL", GUILayout.Width(150)))
        {
            //some functionality
        }
    }

    //loops all levels in the level SO array on the level rig, and generates them based on order, cumulative height, and level distance.
    private void PositionLevels()
    {
        //defines a cumulative height tracker that is incremented at each level iteration
        float cumulativeHeight = 0f;

        //caches a reference to the levels array
        Level[] levels = levelRig.GetLevels;

        for (int i = 0; i < levels.Length; i++)
        {
            //defines a transform 'level', and then assigns it to the child if there is a child at the index, otherwise instantiates and then assigns
            Transform level = default;
            if (i < levelRig.transform.childCount)
            {
                level = levelRig.transform.GetChild(i);
            }
            else
            {
                levelRig.AddLevel();
                level = levelRig.transform.GetChild(i);
            }

            //sets the level position, with a vertical offset for the playerWidth
            level.transform.position = new Vector3(0, cumulativeHeight - playerWidth, 0);

            //positions and expands the backdrop on the basis of the level scriptable object goal as the height using the position formula level height + 1 / 20 goal and scale formula 1/10 goal
            Transform levelBackdrop = level.transform.GetChild(1);
            levelBackdrop.position = level.position + new Vector3(0, levels[i].goal / 2, 0);
            levelBackdrop.localScale = new Vector3(1, levels[i].goal / 10f, 1);

            //applies the material to the level backdrop
            levelBackdrop.GetComponent<SpriteRenderer>().material = levels[i].backdropMaterial;

            //increments the cumulative value using the goal
            cumulativeHeight += levels[i].goal;
        }
    }
}
