using UnityEngine;

public class LevelRig : MonoBehaviour
{

    #region singleton

    public static LevelRig singleton { get; private set; }
    private void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }

    #endregion


    #region placement and scaling

    //allows for retrieval of levels profile from the game manager for placement and scaling in the custom editor
    public GameManager gameManager;

    public Level[] GetLevels
    {
        get
        {
            return gameManager.levels;
        }
    }

    //the template prefab for a level
    public GameObject levelPrefab;

    public void AddLevel()
    {
        Instantiate(levelPrefab, transform);
    }

    #endregion

    //uses the index to select from children objects and returns the position
    public Vector3 GetStartPosition(int index)
    {
        Transform level = transform.GetChild(index);
        return level.position;
    }
}
