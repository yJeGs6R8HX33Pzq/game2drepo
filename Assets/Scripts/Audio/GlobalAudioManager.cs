using UnityEngine;

public class GlobalAudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource button;
    public void PlayButton()
    {
        button.Play();
    }
}
