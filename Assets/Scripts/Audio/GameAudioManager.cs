using UnityEngine;

/// <summary>
/// sound manager for game sfx
/// </summary>
public class GameAudioManager : MonoBehaviour
{
    #region singleton

    public static GameAudioManager singleton { get; private set; }
    private void Awake()
    {
        singleton = this;
    }

    #endregion

    #region initialization

    //subscribes the PlayLevelUp method to the game manager OnLevelUp delegate
    private void Start()
    {
        GameManager.singleton.OnLevelUp += PlayLevelUp;

        GameMode gameMode = GameManager.singleton.gameMode;
        if (gameMode == GameMode.classic || gameMode == GameMode.hero)
        {
            GameManager.singleton.OnGameComplete += PlayGameComplete;
        }
    }

    #endregion

    #region level sounds

    [Header("LEVEL")]
    [SerializeField] private AudioSource levelUp;
    public void PlayLevelUp()
    {
        levelUp.Play();
    }

    [SerializeField] private AudioSource gameComplete;
    public void PlayGameComplete()
    {
        gameComplete.Play();
    }

    #endregion

    #region player sounds

    [Header("PLAYER")]
    [SerializeField] private AudioSource flick;
    public void PlayFlick()
    {
        flick.Play();
    }

    [SerializeField] private AudioSource hit;

    //the minimum and maximum bounds to scale for speed pitch and volume adjustments (speeds below minimum will play min pitch & volume and above maximum will play max pitch & volume)
    [SerializeField] private float minHitSpeed;
    [SerializeField] private float maxHitSpeed;

    //the minimum and maximum volumes
    [SerializeField] private float minHitVolume;
    [SerializeField] private float maxHitVolume;

    //the minimum and maximum pitches
    [SerializeField] private float minHitPitch;
    [SerializeField] private float maxHitPitch;


    public void PlayHit(float speed)
    {
        //defines the percent between the min and max hit speed using the speed
        float percent = (speed - minHitSpeed) / (maxHitSpeed - minHitPitch);

        //uses the percent as interpolant 't' to define the volume between min and max hit volume
        hit.volume = Mathf.Lerp(minHitVolume, maxHitVolume, percent);

        //uses the percent as interpolant 't' to define the pitch between min and max hit pitch
        hit.pitch = Mathf.Lerp(minHitPitch, maxHitPitch, percent);

        //plays the audio source
        hit.Play();
    }

    #endregion

}
