using UnityEngine;
using System.IO;

public class StateManager : MonoBehaviour
{
    public State state;

    #region singleton

    public static StateManager singleton { get; private set; }
    private void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    #endregion

    #region initialization

    private void Start()
    {


        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        Load();
    }

    #endregion

    #region iap

    public void UnlockCharacter(int _id)
    {
        state.charactersUnlocked[_id] = true;
        LobbyManager.singleton.UpdateIAPButtons();
        LobbyManager.singleton.HandlePremium();
    }

    #endregion

    #region save

    private string folderKey = "/GameState/";
    private string fileKey = "MyGameState";

    public void Save()
    {
        //creates a string from the current gameState instance using JSON utility
        string json = JsonUtility.ToJson(state);

        //sets the target folder to the data path + folder key
        string folder = Application.persistentDataPath + folderKey;

        //runs a safety check for the existence of the directory
        if (!Directory.Exists(folder)) { Directory.CreateDirectory(folder); }

        //creates the path string
        string file = fileKey;
        string path = System.IO.Path.Combine(folder, file);

        //clears any preexisting data
        if (File.Exists(path)) { File.Delete(path); }

        //writes the file
        File.WriteAllText(path, json);

        //debug
        Debug.Log("save successful!");
    }


    #endregion

    #region load

    public void Load()
    {
        //assigns the folder, file, and path
        string folder = Application.persistentDataPath + folderKey;
        string file = fileKey;
        string path = System.IO.Path.Combine(folder, file);


        if (File.Exists(path))
        {
            //reads the contents of the file to string 'json'
            string json = File.ReadAllText(path);

            //sets the gameState instance to the appropriate value
            state = JsonUtility.FromJson<State>(json);

            Debug.Log("contents successfully read to gameState instance.");
        }
        else
        {
            state = new State();
            Debug.Log($"no file found, gameState will remain at default values.");
        }
    }

    #endregion

    #region editor

    public void DeleteAllData()
    {
        //simply reinitializes the game state
        state = new State();

        //saves the data
        Save();

        //updates the ui
        LobbyManager.singleton.UpdateIAPButtons();

    }

    #endregion

    private void OnApplicationQuit()
    {
        Save();
    }
}
