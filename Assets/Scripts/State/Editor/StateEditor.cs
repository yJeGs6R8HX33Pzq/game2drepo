using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StateManager))]
public class StateEditor : Editor
{
    StateManager gameStateManager;
    private void OnEnable()
    {
        gameStateManager = (StateManager)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Clear Data"))
        {
            gameStateManager.DeleteAllData();
        }
    }
}
