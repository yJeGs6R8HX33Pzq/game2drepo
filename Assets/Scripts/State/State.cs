public class State
{
    public int highscore;
    public bool[] charactersUnlocked;

    public State()
    {
        //sets the characters unlocked array using a default configuration, with the unlocked characters defined in this template
        charactersUnlocked = new bool[20]
        {
            true,
            true,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        };
    }

    //tbd on 'premium' status or new game modes...
    public bool isPremium;  //amend this as needed...
}
